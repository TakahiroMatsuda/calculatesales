package jp.alhinc.matsuda_takahiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CalculateSales {

	public static void main(String[] args) {
		//コマンドライン引数のエラー処理
		int len = args.length;
		if(len != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		HashMap<String, String> branchmap = new HashMap<String, String>();
		HashMap<String, Long> mapsum = new HashMap<String, Long>();

		if(!branchInput(args[0], "branch.lst", mapsum, branchmap, "支店", "\\d{3}")){
			return;
		}

		//支店定義ファイルの入力と一覧の表示
		BufferedReader br = null;


		//first-repositoryフォルダ内のファイル一覧の読み込み
		File file3 = new File(args[0]);
		File files[] = file3.listFiles();
		//正規表現検索で売り上げファイルのみを選抜
		ArrayList<File> salesList = new ArrayList<File>();


		for (int i = 0 ; i < files.length; i++) {
			String sales = files[i].getName();

			if(files[i].isFile() && sales.matches("[0-9]{8}.rcd")){
				salesList.add(files[i]);
			}
		}
		//連番エラー処理
		for (int i = 0 ; i < salesList.size() - 1; i++){
			// i番目のファイル
			String first = salesList.get(i).getName();
			String[] firstData = first.split("\\." ,0);
			// i + 1
			String second = salesList.get(i + 1).getName();
			String[] secondData = second.split("\\." ,0);


			int code1 = Integer.parseInt(firstData[0]);
			int code2 = Integer.parseInt(secondData[0]);
			if(code2 - code1 != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
		//売り上げファイルの読み込み
		try {
			String line2;
			for (int i = 0 ; i < salesList.size(); i++){
				 FileReader fr = new FileReader(salesList.get(i));
				 br = new BufferedReader(fr);
				 ArrayList<String> rcdReads = new ArrayList<String>();

				 String sales = files[i].getName();

				 while((line2 = br.readLine()) != null) {
					 rcdReads.add(line2);

				 }
				 int size = rcdReads.size();
				 if(size != 2) {
					System.out.println(sales + "のフォーマットが不正です");
					return;
				 }
				 if (!mapsum.containsKey(rcdReads.get(0))){
					 System.out.println(sales + "の支店コードが不正です");
					 return;
				 }
				 long sumA = (mapsum.get(rcdReads.get(0)));
				 String sumB = rcdReads.get(1);
				 //売り上げファイルのエラー処理
				 if(!sumB.matches("\\d+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				 }
				 long sumC = Long.parseLong(sumB);


				 long val = sumA + sumC;
				 int valLen = String.valueOf(val).length();
				 //支店別集計合計金額が10桁超えた時のエラー処理
				 if(valLen > 10) {
					 System.out.println("合計金額が10桁を超えました");
					 return;
				 }

				 mapsum.put(rcdReads.get(0), sumA + sumC);

			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}



		if(!fireOutput(args[0],"branch.out", mapsum, branchmap)){
			return;
		}

	}

	public static boolean fireOutput(String dirPath, String fileName, HashMap<String, Long> sales,HashMap<String, String> names) {
		BufferedWriter bw = null;
		try {
			File file4 = new File(dirPath, fileName);
			FileWriter fw = new FileWriter(file4);
			bw = new BufferedWriter(fw);

			for(String key : sales.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;

		}finally {
			if(bw != null) {
				try {
					bw.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
	public static boolean branchInput(String dirPath, String fileName, HashMap<String, Long> sales, HashMap<String, String> names, String type, String match) {
		File fileE = new File(dirPath, fileName);

		if(!fileE.exists()) {
			System.out.println(type + "定義ファイルが存在しません");
			return false;
		}
		BufferedReader br = null;
		try {
			File file = new File(dirPath, fileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			long number = 0;
			String line;

			while((line = br.readLine()) != null) {
				String str2 = line;
			    String[] branch = str2.split(",", 0);

			  //支店定義ファイルのエラー処理
				if(!branch[0].matches(match)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				int size = branch.length;
				if(size != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				names.put(branch[0],branch[1]);
				sales.put(branch[0],number);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

			}
		}
		return true;
	}
}

